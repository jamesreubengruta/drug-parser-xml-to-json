# Drug Parser - XML to JSON

main.py - main code for parsing the xml file.
small_test.xml - 35 entries xml extracted from master list

# important

atc.json - master list of all atc levels
category.json - master list of all categories
compact.json - desired json format (1 data only)
small_data.json - 35 entries of drug data converted
