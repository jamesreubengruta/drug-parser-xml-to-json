import xml.etree.ElementTree as ET
import json
import sys

def get_atc(xml_file, json_file):
    # Open XML file and parse it
    tree = ET.parse(xml_file)
    root = tree.getroot()


    # Find all drug elements and process them
    drugs = root.findall("drug")
    total_drugs = len(drugs)
    progress = 0
    categories = set()


    for drug in drugs:
        drug_data = {}
        
        # Process drug elements
        for elem in drug:
            if elem.tag == "categories":
                drug_data["categories"] = {
                    "category": [{"category": category.findtext("category"), "mesh-id": category.findtext("mesh-id")} for category in
                                 elem.findall("category")]
                }
                for cat in elem.findall("category"):
                    categories.add(str({"category": cat.findtext("category"), "mesh-id": cat.findtext("mesh-id")}))
            
            
 
        final_levels = {"categories":[]}
        for xx in categories:
            final_levels["categories"].append(xx)


        # Update progress tracker
        progress += 1
        sys.stdout.write("\rProgress: {:.2%}".format(progress / total_drugs))
        sys.stdout.flush()

    # Write drugbank data to JSON file
    with open(json_file, "w") as f:
        json.dump(final_levels, f, indent=4)
    print("\nFetching completed!")


# Usage
get_atc("fd2.xml", "category.json")
