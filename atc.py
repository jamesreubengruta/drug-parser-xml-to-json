import xml.etree.ElementTree as ET
import json
import sys

def get_atc(xml_file, json_file, atc_count_file):
    # Open XML file and parse it
    tree = ET.parse(xml_file)
    root = tree.getroot()

    atc_list = {"atc_list": []}

    # Find all drug elements and process them
    drugs = root.findall("drug")
    total_drugs = len(drugs)
    progress = 0
    atp_levels = set()


    for drug in drugs:
        drug_data = {}
        
        # Process drug elements
        for elem in drug:
            """if elem.tag == "categories":
                drug_data["categories"] = {
                    "category": [{"category": category.findtext("category"), "mesh-id": category.findtext("mesh-id")} for category in
                                 elem.findall("category")]
                }"""

            if elem.tag == "atc-codes":
                atc_codes = elem.findall("atc-code")
                drug_data["atc-codes"] = []
                if atc_codes:  # Check if atc-codes exist
                    for atc_code in atc_codes:
                        atc_data = {
                            "code": atc_code.attrib.get("code", ""),
                            "levels": [{"code": level.attrib.get("code", ""), "name": level.text} for level in atc_code.findall("level")]
                        }
                        for l in atc_code.findall("level"):
                            atp_levels.add(str({"code": l.attrib.get("code", ""), "name": l.text}))
                        drug_data["atc-codes"].append(atc_data)
            
 
        atc_list["atc_list"].append(drug_data["atc-codes"])
        final_levels = {"atc_levels":[]}
        for xx in atp_levels:
            final_levels["atc_levels"].append(xx)


        # Update progress tracker
        progress += 1
        sys.stdout.write("\rProgress: {:.2%}".format(progress / total_drugs))
        sys.stdout.flush()

    # Write drugbank data to JSON file
    with open(json_file, "w") as f:
        json.dump(final_levels, f, indent=4)
    with open(atc_count_file, "w") as f:
        json.dump(atc_list, f, indent=4)
    print("\nFetching completed!")


# Usage
get_atc("fd2.xml", "atc.json","atc_count.json")
