import pymongo
import json

# Connect to MongoDB
client = pymongo.MongoClient("mongodb://localhost:2717/")
db = client["drugs"]
collection = db["mycollection"]

# Load JSON data
with open("small_test.json", "r") as file:
    data = json.load(file)

# Insert JSON data into MongoDB
collection.insert_many(data)

print("Data inserted successfully.")
