import json
import csv

def flatten_json(y):
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '_')
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)
    return out

def json_to_csv(json_file, csv_file):
    with open(json_file, 'r') as f:
        data = json.load(f)

    flattened_data = [flatten_json(entry) for entry in data]

    with open(csv_file, 'w', newline='') as f:
        writer = csv.DictWriter(f, fieldnames=flattened_data[0].keys())
        writer.writeheader()
        writer.writerows(flattened_data)

if __name__ == "__main__":
    json_file = input("Enter JSON file name: ")
    csv_file = input("Enter CSV file name: ")
    json_to_csv(json_file, csv_file)
