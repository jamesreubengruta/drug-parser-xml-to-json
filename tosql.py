import json

def generate_sql_from_json(json_data, sql_file):
    with open(sql_file, 'w') as f:
        # Create Drug table
        f.write("CREATE TABLE Drug (\n")
        f.write("    drugbank_id VARCHAR(255) PRIMARY KEY,\n")
        f.write("    type VARCHAR(255),\n")
        f.write("    created VARCHAR(255),\n")
        f.write("    updated VARCHAR(255)\n")
        f.write(");\n\n")

        # Create Target table
        f.write("CREATE TABLE Target (\n")
        f.write("    target_id INT AUTO_INCREMENT PRIMARY KEY,\n")
        f.write("    drugbank_id VARCHAR(255),\n")
        f.write("    name VARCHAR(255),\n")
        f.write("    organism VARCHAR(255),\n")
        f.write("    FOREIGN KEY (drugbank_id) REFERENCES Drug(drugbank_id)\n")
        f.write(");\n\n")

        # Create Enzyme table
        f.write("CREATE TABLE Enzyme (\n")
        f.write("    enzyme_id INT AUTO_INCREMENT PRIMARY KEY,\n")
        f.write("    drugbank_id VARCHAR(255),\n")
        f.write("    uniprot_id VARCHAR(255),\n")
        f.write("    FOREIGN KEY (drugbank_id) REFERENCES Drug(drugbank_id)\n")
        f.write(");\n\n")

        # Insert data into Drug table
        for drug in json_data['drugbank']['drug']:
            f.write("INSERT INTO Drug (drugbank_id, type, created, updated) VALUES ('{}', '{}', '{}', '{}');\n"
                    .format(drug.get('drugbank-id', ''), drug.get('type', ''), drug.get('created', ''), drug.get('updated', '')))

        # Insert data into Target table
        for drug in json_data['drugbank']['drug']:
            if 'targets' in drug:
                for target in drug['targets']:
                    f.write("INSERT INTO Target (drugbank_id, name, organism) VALUES ('{}', '{}', '{}');\n"
                            .format(drug.get('drugbank-id', ''), target.get('name', ''), target.get('organism', '')))

        # Insert data into Enzyme table
        for drug in json_data['drugbank']['drug']:
            if 'enzymes' in drug:
                for enzyme in drug['enzymes']:
                    f.write("INSERT INTO Enzyme (drugbank_id, uniprot_id) VALUES ('{}', '{}');\n"
                            .format(drug.get('drugbank-id', ''), enzyme.get('uniprot-id', '')))

        # Repeat similar process for other tables

# Load JSON data from file
with open("ultra.json", 'r') as json_file:
    data = json.load(json_file)

# Generate SQL file
generate_sql_from_json(data, "output.sql")
